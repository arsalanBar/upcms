//import './firebase'

import Vue from 'vue'

import VueFire from 'vuefire'
//mport Firebase from 'firebase'


import Buefy from 'buefy'
import 'buefy/lib/buefy.css'

//import 'vue-chartjs'
//import VueChartJS from 'vue-chartjs'
//import VueCharts from 'hchs-vue-charts'
//import VueChartJS from '@/components/VueChartJS' //The VueChartJS component that displays the vue-chartjs charts.

import App from './App.vue'
Vue.use(VueFire)
Vue.use(Buefy)

//Vue.use(VueCharts);
//Vue.use(VueChartsJS);
import Chartkick from 'chartkick'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'

Vue.use(VueChartkick, { Chartkick })
// OR

Vue.component(Buefy.Checkbox.name, Buefy.Checkbox)
Vue.component(Buefy.Table.name, Buefy.Table)
Vue.component(Buefy.Switch.name, Buefy.Switch)



new Vue({
  el: '#app',
  render: h => h(App)
})
